﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System.Text;

namespace FA.JustBlog.UnitTest
{
    public class PostRespositoryTest
    {
        private PostRepository _respository;
        private readonly JustBlogContext _context = new();
        [SetUp]
        public void Setup()
        {
            _respository = new PostRepository();
        }
        [Test]
        public void AddPostSuccess()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.DoesNotThrow(() => _respository.AddPost(post));
        }
        [Test]
        public void DeletePostByIdSuccess()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            Assert.DoesNotThrow(() => _respository.DeletePost(post.Id));
        }
        [Test]
        public void DeletePostSuccess()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            Assert.DoesNotThrow(() => _respository.DeletePost(post));
        }
        [Test]
        public void TestCountCategorySuccess()
        {
            Assert.DoesNotThrow(() => _respository.CountPostsForCategory("ASP NET"));
        }
        [Test]
        public void TestCountTagSuccess()
        {
            Assert.DoesNotThrow(() => _respository.CountPostsForTag("ASP NET"));
        }
        [Test]
        public void UpdatePostSuccess()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.Title = "Test update";
            Assert.DoesNotThrow(() => _respository.UpdatePost(post));
        }
        [Test]
        public void AddPostTitleOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 257)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = testString,
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostShortDescriptionOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 1026)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = testString,
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostPostContentOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 4005)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = testString,
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostUrlSlugOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 257)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = testString,
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void UpdatePostTitleOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 257)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.Title = testString;
            Assert.Throws<Exception>(() => _respository.UpdatePost(post));
        }
        [Test]
        public void UpdatePostShortDescriptionOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 1026)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.ShortDescription = testString;
            Assert.Throws<Exception>(() => _respository.UpdatePost(post));
        }
        [Test]
        public void UpdatePostPostContentOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 4005)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.PostContent = testString;
            Assert.Throws<Exception>(() => _respository.UpdatePost(post));
        }
        [Test]
        public void UpdatePostUrlSlugOutOfLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int length = 0;
            while (length < 257)
            {
                stringBuilder.Append("a");
                length++;
            }
            string testString = stringBuilder.ToString();
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.UrlSlug = testString;
            Assert.Throws<Exception>(() => _respository.UpdatePost(post));
        }
        [Test]
        public void UpdatePostCategoryNotFound()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            _respository.AddPost(post);
            post.CategoryId = 100;
            Assert.Throws<Exception>(() => _respository.UpdatePost(post));
        }
        [Test]
        public void DeletePostByIdNotFound()
        {
            Assert.Throws<Exception>(() => _respository.DeletePost(int.MaxValue));
        }
        [Test]
        public void FindPostSuccess()
        {
            Assert.DoesNotThrow(() => _respository.FindPost(1));
        }
        [Test]
        public void FindPostNotFound()
        {
            Assert.Throws<Exception>(() => _respository.FindPost(int.MaxValue));
        }
        [Test]
        public void GetPublishedPostsSuccess()
        {
            var publishedPost = _context.Posts.Where(x => x.Published).Count();
            Assert.AreEqual(_respository.GetPublishedPosts().Count, publishedPost);
        }
        [Test]
        public void GetUnpublishedPostsSuccess()
        {
            var unpublishedPost = _context.Posts.Where(x => !x.Published).Count();
            Assert.AreEqual(_respository.GetUnpublisedPosts().Count, unpublishedPost);
        }
        [Test]
        public void GetListSuccess()
        {
            int count = _context.Posts.Count();
            Assert.AreEqual(_respository.GetAllPosts().Count, count);
        }
        [Test]
        public void GetListByCategorySuccess()
        {
            int count = _context.Posts.Where(x => x.CategoryId == 2).Count();
            Assert.AreEqual(_respository.GetPostsByCategory("ASP NET").Count, count);
        }
        [Test]
        public void GetListByTagSuccess()
        {
            int count = _context.PostTagsMap.Where(x => x.TagId == 2).Count();
            Assert.AreEqual(_respository.GetPostsByTag("ASP NET").Count, count);
        }
        [Test]
        public void GetListByMonthSuccess()
        {
            int count = _context.Posts.Where(x => x.PostedOn.Month == 1).Count();
            Assert.AreEqual(_respository.GetPostsByMonth(new DateTime(2002, 1, 12)).Count(), count);
        }
        [Test]
        public void AddPostFailRequiredTitleField()
        {
            Post post = new Post()
            {
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostFailRequiredShortDescriptionField()
        {
            Post post = new Post()
            {
                Title = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostFailRequiredPostContentField()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostFailRequiredUrlSlugField()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostFailCategoryNotExist()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 0
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostTitleSpecialCharacter()
        {
            Post post = new Post()
            {
                Title = "< > ~ ! @ # $ % ^ & * ",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostShortDescriptionSpecialCharacter()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "< > ~ ! @ # $ % ^ & * ",
                PostContent = "Test",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostPostContentSpecialCharacter()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "< > ~ ! @ # $ % ^ & * ",
                UrlSlug = "Test",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void AddPostUrlSlugSpecialCharacter()
        {
            Post post = new Post()
            {
                Title = "Test",
                ShortDescription = "Test",
                PostContent = "Test",
                UrlSlug = "< > ~ ! @ # $ % ^ & * ",
                Published = true,
                PostedOn = DateTime.Now,
                CategoryId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddPost(post));
        }
        [Test]
        public void UpdatePostTitleSpecialCharacter()
        {
            var postUpdate = _respository.FindPost(1);
            postUpdate.Title = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdatePost(postUpdate));
        }
        [Test]
        public void UpdatePostShortDescriptionSpecialCharacter()
        {
            var postUpdate = _respository.FindPost(1);
            postUpdate.ShortDescription = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdatePost(postUpdate));
        }
        [Test]
        public void UpdatePostPostContentSpecialCharacter()
        {
            var postUpdate = _respository.FindPost(1);
            postUpdate.PostContent = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdatePost(postUpdate));
        }
        [Test]
        public void UpdatePostUrlSlugSpecialCharacter()
        {
            var postUpdate = _respository.FindPost(1);
            postUpdate.UrlSlug = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdatePost(postUpdate));
        }
        [Test]
        public void AddPostInvalidDateTime()
        {
            try
            {
                Post post = new Post()
                {
                    Title = "test",
                    ShortDescription = "test",
                    PostContent = "test",
                    UrlSlug = "test",
                    Published = true,
                    PostedOn = DateTime.Parse("31/02/2024"),
                    CategoryId = 1
                };
                Assert.Throws<Exception>(() => _respository.AddPost(post));
            }
            catch (Exception)
            {

                Assert.Throws<FormatException>(() => throw new FormatException());
            }
        }
        [Test]
        public void TestGetMostViewByPost()
        {
            var count = _context.Posts.Count();
            Assert.AreEqual(count, _respository.GetMostViewedPost(count).Count());
        }
        [Test]
        public void TestTotalHighestPost()
        {
            var count = _context.Posts.Count();
            Assert.AreEqual(count, _respository.GetHighestPosts(count).Count());
        }
    }
}
