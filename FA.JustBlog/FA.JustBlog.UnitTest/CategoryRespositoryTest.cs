﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System.Text;

namespace FA.JustBlog.UnitTest
{
    public class CategoryRespositoryTest
    {
        private CategoryRepository _respository;
        private readonly JustBlogContext _context = new();
        [SetUp]
        public void Setup()
        {
            _respository = new CategoryRepository();
        }
        [Test]
        public void AddCategorySuccess()
        {
            Category category = new Category()
            {
                Name = "Test",
                UrlSlug = "Test",
                Description = "Test",
            };
            Assert.DoesNotThrow(() => _respository.AddCategory(category));
        }
        [Test]
        public void DeleteCategoryByIdSuccess()
        {
            var contextDelete = _context.Categories.FirstOrDefault(x => x.Name == "Test");
            Assert.DoesNotThrow(() => _respository.DeleteCategory(contextDelete!.Id));
        }
        [Test]
        public void DeleteCategorySuccess()
        {
            Category category = new Category()
            {
                Name = "Delete category",
                UrlSlug = "Test",
                Description = "Test",
            };
            _respository.AddCategory(category);
            Assert.DoesNotThrow(() => _respository.DeleteCategory(category));
        }
        [Test]
        public void FindCategoryByIdSuccess()
        {
            Assert.DoesNotThrow(() => _respository.Find(1));
        }
        [Test]
        public void GetListSuccess()
        {
            int count = _context.Categories.Count();
            Assert.AreEqual(_respository.GetAllCategories().Count, count);
        }
        [Test]
        public void UpdateSucess()
        {
            Category categoryUpdate = _respository.Find(1);
            categoryUpdate.Name = "Test update";
            Assert.DoesNotThrow(() => _respository.UpdateCategory(categoryUpdate));
        }
        [Test]
        public void AddFailRequiredNameField()
        {
            Category category = new Category()
            {
                Description = "Test",
                UrlSlug = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void AddFailRequiredDescriptionField()
        {
            Category category = new Category()
            {
                Name = "Test",
                UrlSlug = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void AddFailRequiredUrlSlugField()
        {
            Category category = new Category()
            {
                Name = "Test",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void DeleteIdNotFound()
        {
            Assert.Throws<Exception>(() => _respository.DeleteCategory(int.MaxValue));
        }
        [Test]
        public void GetItemNotExist()
        {
            Assert.Throws<Exception>(() => _respository.Find(int.MaxValue));
        }
        [Test]
        public void AddItemNameOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Category category = new Category()
            {
                Name = testString,
                UrlSlug = "test",
                Description = "Test"
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void AddItemUrlSlugOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Category category = new Category()
            {
                Name = "test",
                UrlSlug = testString,
                Description = "Test"
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void AddItemUrlDescriptionOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 1026)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Category category = new Category()
            {
                Name = "test",
                UrlSlug = "test",
                Description = testString
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void UpdateItemNameOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var updateCategory = _respository.Find(1);
            updateCategory.Name = testString;
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));

        }
        [Test]
        public void UpdateItemUrlSlugOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var updateCategory = _respository.Find(1);
            updateCategory.UrlSlug = testString;
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));

        }
        [Test]
        public void UpdateItemDesctiptionOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 1027)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var updateCategory = _respository.Find(1);
            updateCategory.Description = testString;
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));

        }
        [Test]
        public void AddCategoryNameWithSpecialCharacter()
        {
            Category category = new Category()
            {
                Name = "< > ~ ! @ # $ % ^ & * ",
                UrlSlug = "Test",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));

        }
        [Test]
        public void AddCategoryUrlSlugSpecialCharacter()
        {
            Category category = new Category()
            {
                Name = "Test",
                UrlSlug = "< > ~ ! @ # $ % ^ & * ",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));

        }
        [Test]
        public void AddCategoryDescriptionSpecialCharacter()
        {
            Category category = new Category()
            {
                Name = "Test",
                UrlSlug = "Test",
                Description = "< > ~ ! @ # $ % ^ & * ",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }
        [Test]
        public void UpdateCategoryNameSpecialCharacter()
        {
            var updateCategory = _respository.Find(1);
            updateCategory.Name = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));
        }
        [Test]
        public void UpdateCategoryUrlSlugSpecialCharacter()
        {
            var updateCategory = _respository.Find(1);
            updateCategory.UrlSlug = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));
        }
        [Test]
        public void UpdateCategoryDescriptionSpecialCharacter()
        {
            var updateCategory = _respository.Find(1);
            updateCategory.Description = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateCategory(updateCategory));
        }

    }
}
