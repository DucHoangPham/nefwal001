﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System.Text;

namespace FA.JustBlog.UnitTest
{
    public class TagRespositoryTest
    {
        private TagRepository _respository;
        private readonly JustBlogContext _context = new();
        [SetUp]
        public void Setup()
        {
            _respository = new TagRepository();
        }
        [Test]
        public void AddTagSuccess()
        {
            Tag tag = new Tag()
            {
                Name = "test",
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            Assert.DoesNotThrow(() => _respository.AddTag(tag));
        }
        [Test]
        public void DeleteTagByIdSuccess()
        {
            Tag tag = new Tag()
            {
                Name = "test",
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            _respository.AddTag(tag);
            Assert.DoesNotThrow(() => _respository.DeleteTag(tag.Id));
        }
        [Test]
        public void DeleteTagSuccess()
        {
            Tag tag = new Tag()
            {
                Name = "test",
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            _respository.AddTag(tag);
            Assert.DoesNotThrow(() => _respository.DeleteTag(tag));
        }
        [Test]
        public void FindTagByIdSuccess()
        {
            Assert.DoesNotThrow(() => _respository.Find(1));
        }
        [Test]
        public void GetListSuccess()
        {
            int count = _context.Tags.Count();
            Assert.AreEqual(_respository.GetAllTags().Count, count);
        }
        [Test]
        public void UpdateSucess()
        {
            Tag tagUpdate = _respository.Find(1);
            tagUpdate.Name = "Test update";
            Assert.DoesNotThrow(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void AddFailRequiredNameField()
        {
            Tag tag = new Tag()
            {
                Description = "Test",
                UrlSlug = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void AddFailRequiredDescriptionField()
        {
            Tag tag = new Tag()
            {
                Name = "Test",
                UrlSlug = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void AddFailRequiredUrlSlugField()
        {
            Tag tag = new Tag()
            {
                Name = "Test",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void DeleteTagFail()
        {
            Assert.Throws<Exception>(() => _respository.DeleteTag(0));
        }
        [Test]
        public void FindTagByIdFail()
        {
            Assert.Throws<Exception>(() => _respository.Find(0));
        }
        [Test]
        public void AddTagNameOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Tag tag = new Tag()
            {
                Name = testString,
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void AddTagUrlSlugOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Tag tag = new Tag()
            {
                Name = "test",
                UrlSlug = testString,
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void AddTagDescriptionOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 1026)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Tag tag = new Tag()
            {
                Name = "test",
                UrlSlug = "test",
                Description = testString,
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void UpdateTagNameOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var tagUpdate = _respository.Find(1);
            tagUpdate.Name = testString;
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void UpdateTagUrlSlugOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 257)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var tagUpdate = _respository.Find(1);
            tagUpdate.UrlSlug = testString;
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void UpdateTagDescriptionOutOfMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 1027)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            var tagUpdate = _respository.Find(1);
            tagUpdate.Description = testString;
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void AddTagNameSpecialCharacter()
        {
            Tag tag = new Tag()
            {
                Name = "< > ~ ! @ # $ % ^ & * ",
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));

        }
        [Test]
        public void AddTagUrlSlugSpecialCharacter()
        {
            Tag tag = new Tag()
            {
                Name = "Test ",
                UrlSlug = "< > ~ ! @ # $ % ^ & * ",
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void AddTagDescriptionSpecialCharacter()
        {
            Tag tag = new Tag()
            {
                Name = "Test ",
                UrlSlug = "Test",
                Description = "< > ~ ! @ # $ % ^",
                Count = 1
            };
            Assert.Throws<Exception>(() => _respository.AddTag(tag));
        }
        [Test]
        public void UpdateTagNameSpecialCharacter()
        {
            var tagUpdate = _respository.Find(1);
            tagUpdate.Name = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void UpdateTagUrlSlugSpecialCharacter()
        {
            var tagUpdate = _respository.Find(1);
            tagUpdate.UrlSlug = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void UpdateTagDescriptionSpecialCharacter()
        {
            var tagUpdate = _respository.Find(1);
            tagUpdate.Description = "< > ~ ! @ # $ % ^ & * ";
            Assert.Throws<Exception>(() => _respository.UpdateTag(tagUpdate));
        }
        [Test]
        public void AddTagInvalidCount()
        {
            try
            {
                Tag tag = new Tag()
                {
                    Name = "Test",
                    UrlSlug = "Test",
                    Description = "Test",
                    Count = int.Parse("xx")
                };
                Assert.Throws<Exception>(() => _respository.AddTag(tag));
            }
            catch (Exception)
            {

                Assert.Throws<FormatException>(() => throw new FormatException());
            }
        }
    }
}
