﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System.Text;

namespace FA.JustBlog.UnitTest
{
    public class CommentRespositoryTest
    {
        private CommentRespository _respository;
        private readonly JustBlogContext _context = new();
        [SetUp]
        public void SetUp()
        {
            _respository = new CommentRespository();
        }
        [Test]
        public void AddCommentSuccess()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "Test@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.DoesNotThrow(() => _respository.AddComment(comment));
        }
        [Test]
        public void DeleteCommentByIdSuccess()
        {
            Comment commentDelete = new Comment()
            {
                Name = "Delete comment",
                Email = "delete@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            _respository.AddComment(commentDelete);
            Assert.DoesNotThrow(() => _respository.DeleteComment(commentDelete.Id));
        }
        [Test]
        public void DeleteCommentSuccess()
        {
            Comment commentDelete = new Comment()
            {
                Name = "Delete comment",
                Email = "delete@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            _respository.AddComment(commentDelete);
            Assert.DoesNotThrow(() => _respository.DeleteComment(commentDelete));
        }
        [Test]
        public void FindCommentByIdSuccess()
        {
            Comment comment = new Comment()
            {
                Name = "Delete comment",
                Email = "delete@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            _respository.AddComment(comment);
            Assert.DoesNotThrow(() => _respository.FindComment(comment.Id));
        }
        [Test]
        public void UpdateCommentSuccess()
        {
            Comment comment = new Comment()
            {
                Name = "Delete comment",
                Email = "delete@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            _respository.AddComment(comment);
            Comment commentUpdate = _respository.FindComment(comment.Id);
            commentUpdate.Name = "update";
            Assert.DoesNotThrow(() => _respository.UpdateComment(commentUpdate));
        }
        [Test]
        public void AddCommentSuccessByProperty()
        {
            Assert.DoesNotThrow(() => _respository.AddComment(1, "Test", "abc@gmail.com", "Test", "Test"));
        }
        [Test]
        public void GetAllComment()
        {
            int count = _context.Comments.Count();
            Assert.AreEqual(_respository.GetAllComments().Count, count);
        }
        [Test]
        public void GetCommentByPostId()
        {
            int postId = 1;
            int count = _context.Comments.Where(x => x.PostId == postId).Count();
            Assert.AreEqual(_respository.GetCommentsForPost(postId).Count, count);
        }
        [Test]
        public void GetCommentByPost()
        {
            Post post = _context.Posts.FirstOrDefault(x => x.Id == 1)!;
            int count = _context.Comments.Where(x => x.PostId == post.Id).Count();
            Assert.AreEqual(_respository.GetCommentsForPost(post).Count, count);
        }
        [Test]
        public void AddCommentFailRequiredNameField()
        {
            Comment comment = new Comment()
            {
                Email = "Test@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailRequiredEmailField()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailRequiredCommentHeaderField()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "Test@test.com",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailRequiredCommentTextField()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "Test@test.com",
                CommentHeader = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void CommentFailPostIdNotFound()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "Test@test.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = int.MaxValue
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void DeleteCommentFailNotFound()
        {
            Assert.Throws<Exception>(() => _respository.DeleteComment(int.MaxValue));
        }
        [Test]
        public void UpdateCommentFailNotFound()
        {
            Comment comment = new Comment()
            {
                Id = int.MaxValue,
                Name = "Test",
                Email = "ccc@test",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void FindCommentByIdFailNotFound()
        {
            Assert.Throws<Exception>(() => _respository.FindComment(int.MaxValue));
        }
        [Test]
        public void AddCommentFailNameOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = testString,
                Email = "test@gmail.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailEmailOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 253)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = testString + "@gmail.com.vn",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailCommentHeaderOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = testString,
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailCommentTextOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 4005)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "Test",
                CommentText = testString,
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailTimeText()
        {
            try
            {
                Comment comment = new Comment()
                {
                    Name = "Test",
                    Email = "test@gmail.com",
                    CommentHeader = "Test",
                    CommentText = "Test",
                    CommentTime = DateTime.Parse("31/02/2024"),
                    PostId = 1
                };
                Assert.Throws<Exception>(() => _respository.AddComment(comment));
            }
            catch (Exception)
            {

                Assert.Throws<FormatException>(() => throw new FormatException());
            }
        }
        [Test]
        public void UpdateCommentFailNameOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = testString,
                Email = "test@gmail.com",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailEmailOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 253)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = testString + "@gmail.com.vn",
                CommentHeader = "Test",
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailCommentHeaderOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = testString,
                CommentText = "Test",
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailCommentTextOutMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 4005)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "Test",
                CommentText = testString,
                CommentTime = DateTime.Now,
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailTimeText()
        {
            try
            {
                Comment comment = new Comment()
                {
                    Name = "Test",
                    Email = "test@gmail.com",
                    CommentHeader = "Test",
                    CommentText = "Test",
                    CommentTime = DateTime.Parse("31/02/2024"),
                    PostId = 1
                };
                Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
            }
            catch (Exception)
            {
                Assert.Throws<FormatException>(() => throw new FormatException());
            }
        }
        [Test]
        public void AddCommentFailNameRequiredWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, null, "test@test.com", "test", "test"));
        }
        [Test]
        public void AddCommentFailEmailRequiredWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", null, "test", "test"));
        }
        [Test]
        public void AddCommentFailCommentHeaderRequiredWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", null, "test"));
        }
        [Test]
        public void AddCommentFailCommentTextRequiredWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", "test", null));
        }
        [Test]
        public void AddCommentFailNameOutMaxLengthWithOtherCreateFunction()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Assert.Throws<Exception>(() => _respository.AddComment(1, testString, "test@game.com", "test", "test"));
        }
        [Test]
        public void AddCommentFailEmailOutMaxLengthWithOtherCreateFunction()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 253)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", testString + "@gmail.com", "test", "test"));
        }
        [Test]
        public void AddCommentFailCommentHeaderOutMaxLengthWithOtherCreateFunction()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 256)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", testString, "test"));
        }
        [Test]
        public void AddCommentFailCommentTextOutMaxLengthWithOtherCreateFunction()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 4005)
            {
                stringBuilder.Append("a");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", "test", testString));
        }
        [Test]
        public void AddFailPostIdNotFoundWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(int.MaxValue, "test", "test@gmail.com", "test", "test"));
        }
        [Test]
        public void AddCommentFailNameRegex()
        {
            Comment comment = new Comment()
            {
                Name = "< > ~ ! @ # $ % ^ & *",
                Email = "Test@gmail.com",
                CommentHeader = "test",
                CommentText = "test",
                PostId = 1,
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailEmailRegex()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "< > ~ ! @ # $ % ^ & *",
                CommentHeader = "test",
                CommentText = "test",
                PostId = 1,
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailCommentHeaderRegex()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "< > ~ ! @ # $ % ^ & *",
                CommentText = "test",
                PostId = 1,
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailCommentTextRegex()
        {
            Comment comment = new Comment()
            {
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "test",
                CommentText = "< > ~ ! @ # $ % ^ & *",
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.AddComment(comment));
        }
        [Test]
        public void AddCommentFailNameRegexWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "< > ~ ! @ # $ % ^ & *", "test@gmail.com", "test", "test"));
        }
        [Test]
        public void AddCommentFailEmailRegexWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "< > ~ ! @ # $ % ^ & *", "test", "test"));
        }
        [Test]
        public void AddCommentFailCommentHeaderRegexWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", "< > ~ ! @ # $ % ^ & *", "test"));
        }
        [Test]
        public void AddCommentFailCommentTextRegexWithOtherCreateFunction()
        {
            Assert.Throws<Exception>(() => _respository.AddComment(1, "test", "test@gmail.com", "test", "< > ~ ! @ # $ % ^ & *"));
        }
        [Test]
        public void UpdateCommentFailNameRegex()
        {
            Comment comment = new Comment()
            {
                Id = 1,
                Name = "< > ~ ! @ # $ % ^ & *",
                Email = "test@gmail.com",
                CommentHeader = "test",
                CommentText = "test",
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailEmailRegex()
        {
            Comment comment = new Comment()
            {
                Id = 1,
                Name = "Test",
                Email = "< > ~ ! @ # $ % ^ & *",
                CommentHeader = "test",
                CommentText = "test",
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailCommentHeaderRegex()
        {
            Comment comment = new Comment()
            {
                Id = 1,
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "< > ~ ! @ # $ % ^ & *",
                CommentText = "test",
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
        [Test]
        public void UpdateCommentFailCommentTextRegex()
        {
            Comment comment = new Comment()
            {
                Id = 1,
                Name = "Test",
                Email = "test@gmail.com",
                CommentHeader = "test",
                CommentText = "< > ~ ! @ # $ % ^ & *",
                PostId = 1
            };
            Assert.Throws<Exception>(() => _respository.UpdateComment(comment));
        }
    }
}
