﻿$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#myTable').DataTable({
        "ajax": { url: '/Admin/Post/GetAllPostJson' },
        "columns": [
            { "data": "title", "width": "15%" },
            { "data": "shortDescription", "width": "15%" },
            { "data": "urlSlug", "width": "10%" },
            { "data": "postedOn", "width": "10%" },
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                            <a href="admin/post/details/${data}" class="btn btn-primary text-white" style='cursor:pointer; '>
                                    Details</a>
                                <a href="/post/edit/${data}" class='btn btn-success text-white' style='cursor:pointer; '>
                                    Edit
                                </a>
                                &nbsp;
                                <a href="admin/Post/Delete/${data}" class='btn btn-danger text-white' style='cursor:pointer; '>
                                    Delete
                                </a>
                            </div>`;
                },
            "width": "50%"
            }
        ],
        initComplete: function () {
            this.api()
                .columns()
                .every(function () {
                    let column = this;
                    let title = column.footer().textContent;

                    // Create input element
                    let input = document.createElement('input');
                    input.placeholder = title;
                    column.footer().replaceChildren(input);

                    // Event listener for user input
                    input.addEventListener('keyup', () => {
                        if (column.search() !== this.value) {
                            column.search(input.value).draw();
                        }
                    });
                });
        }
    })
}