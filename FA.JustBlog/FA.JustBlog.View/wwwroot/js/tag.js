﻿$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#tagTable').DataTable({
        "ajax": { url: '/admin/tag/getalltagsjson' },
        "columns": [
            { "data": "name", "width": "20%" },
            { "data": "urlSlug", "width": "20%" },
            { "data": "count", "width": "20%" },
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                                <a href="tag/details/${data}" class="btn btn-primary text-white" style='cursor:pointer; '>Details</a>
                                <a href="/tag/edit/${data}" class='btn btn-success text-white' style='cursor:pointer; '>
                                    Edit
                                </a>
                                &nbsp;
                                <a href="/tag/Delete/${data}" class='btn btn-danger text-white' style='cursor:pointer; '>
                                    Delete
                                </a>
                            </div>`;
                },
                "width": "40%"
            }
        ],
        initComplete: function () {
            this.api()
                .columns()
                .every(function () {
                    let column = this;
                    let title = column.footer().textContent;

                    // Create input element
                    let input = document.createElement('input');
                    input.placeholder = title;
                    column.footer().replaceChildren(input);

                    // Event listener for user input
                    input.addEventListener('keyup', () => {
                        if (column.search() !== this.value) {
                            column.search(input.value).draw();
                        }
                    });
                });
        }
    })
}