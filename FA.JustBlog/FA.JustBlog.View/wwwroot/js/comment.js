﻿$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#commentTable').DataTable({
        "ajax": { url: '/admin/comment/getallcommentsjson' },
        "columns": [
            { "data": "name", "width": "15%" },
            { "data": "email", "width": "15%" },
            { "data": "commentHeader", "width": "15%" },
            { "data": "commentTime", "width": "15%"},
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                                <a href="comment/details/${data}" class="btn btn-primary text-white" style='cursor:pointer; '>Details</a>
                                <a href="/comment/edit/${data}" class='btn btn-success text-white' style='cursor:pointer; '>
                                    Edit
                                </a>
                                &nbsp;
                                <a href="/comment/Delete/${data}" class='btn btn-danger text-white' style='cursor:pointer; '>
                                    Delete
                                </a>
                            </div>`;
                },
                "width": "40%"
            }
        ],
        initComplete: function () {
            this.api()
                .columns()
                .every(function () {
                    let column = this;
                    let title = column.footer().textContent;

                    // Create input element
                    let input = document.createElement('input');
                    input.placeholder = title;
                    column.footer().replaceChildren(input);

                    // Event listener for user input
                    input.addEventListener('keyup', () => {
                        if (column.search() !== this.value) {
                            column.search(input.value).draw();
                        }
                    });
                });
        }
    })
}