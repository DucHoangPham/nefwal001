﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FA.JustBlog.View.HTMLHelper
{
    public static class PostExtentions
    {

        public static Category GetCategory(this IHtmlHelper html, Post post)
        {
            PostRepository postRepository = new PostRepository();
            var postCategory = postRepository.GetCategoryByPostId(post.Id);
            return postCategory;
        }
        public static IList<Tag> GetTags(this IHtmlHelper html, Post post)
        {
            PostRepository postRepository = new PostRepository();
            var postTags = postRepository.GetTagsForPost(post.Id);
            return postTags;
        }
        public static string GetFriendlyDateTimeValue(this IHtmlHelper html, Post post)
        {
            DateTime dateTime = post.PostedOn;
            DateTime timeNow = DateTime.Now;
            var diff = timeNow - dateTime;
            if (diff.TotalSeconds < 60)
            {
                return $"{(int)diff.TotalSeconds} seconds ago";
            }

            if (diff.TotalMinutes < 60)
            {
                return $"{(int)diff.TotalMinutes} minutes ago";
            }

            if (diff.TotalHours < 24)
            {
                return $"{(int)diff.TotalHours} hours ago";
            }

            if (diff.TotalDays < 7)
            {
                if (diff.TotalDays < 2)
                {
                    return "Yesterday";
                }
                return $"{(int)diff.TotalDays} days ago";
            }

            return dateTime.ToString("d MMM yyyy 'at' h:mm tt");
        }


    }
}
