﻿using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.View.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
