﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.View.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.USER_ROLE}, {SD.BLOGOWNER_ROLE}")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepository _categoryRespository;

        public CategoryController()
        {
            _categoryRespository = new CategoryRepository();
        }

        // GET: Category

        public IActionResult Index()
        {
            var lstPost = _categoryRespository.GetAllCategories();
            return View(lstPost);
        }

        // GET: Category/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = _categoryRespository.Find((int)id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        //Get category by name
        public IActionResult GetCategoryByName(string name)
        {
            var category = _categoryRespository.Find(name);
            return View("Details", category);
        }

        // GET: Category/Create
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE},  {SD.BLOGOWNER_ROLE}")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,UrlSlug,Description")] Category category)
        {
            if (ModelState.IsValid)
            {
                _categoryRespository.AddCategory(category);
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Category/Edit/5
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE},  {SD.BLOGOWNER_ROLE}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = _categoryRespository.Find((int)id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,UrlSlug,Description")] Category category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _categoryRespository.UpdateCategory(category);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Category/Delete/5
        [Authorize(Roles = SD.BLOGOWNER_ROLE)]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = _categoryRespository.Find((int)id);
            if (category == null)
            {
                return NotFound();
            }
            _categoryRespository.DeleteCategory(category);
            return RedirectToAction(nameof(Index));
        }

        #region API call
        public IActionResult GetAllCategoryJson()
        {
            var lstCategory = _categoryRespository.GetAllCategories();
            return Json(new { data = lstCategory });
        }
        #endregion


    }
}
