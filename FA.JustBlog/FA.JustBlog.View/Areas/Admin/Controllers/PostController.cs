﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FA.JustBlog.View.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.USER_ROLE}, {SD.BLOGOWNER_ROLE}")]
    public class PostController : Controller
    {
        private readonly PostRepository _postRespository;
        private readonly CategoryRepository _categoryRepository;
        public PostController()
        {
            _postRespository = new PostRepository();
            _categoryRepository = new CategoryRepository();
        }

        // GET: Post
        public IActionResult Index()
        {
            var lstPost = _postRespository.GetAllPosts();
            return View(lstPost);
        }

        //Get post order by PostOn

        public IActionResult PostOrderByPostOn()
        {
            var lstPost = _postRespository.GetAllPosts().OrderBy(x => x.PostedOn);
            return View("Index", lstPost);
        }

        //Get MostViewedPosts

        public IActionResult MostViewedPosts()
        {

            return View();
        }

        //Get most 5 interested post

        public IActionResult MostInterestedPosts()
        {
            return View();
        }

        //Get published post
        [Authorize(Roles = $" {SD.BLOGOWNER_ROLE}")]

        public IActionResult GetPublishedPost()
        {
            return View();
        }
        [Authorize(Roles = $" {SD.BLOGOWNER_ROLE}")]
        //Get Unpublished pos
        public IActionResult GetUnpublishedPost()
        {
            return View();
        }

        //Get post by category

        public IActionResult PostByCategory(string categoryName)
        {
            var lstPost = _postRespository.GetPostsByCategory(categoryName);
            return View("Index", lstPost);
        }



        //Get post by tag

        public IActionResult GetPostByTag(string tagName)
        {
            var lstPost = _postRespository.GetPostsByTag(tagName);
            return View("Index", lstPost);
        }



        // GET: Post/Details/5

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _postRespository.FindPost((int)id);
            var category = _categoryRepository.Find(post.CategoryId);
            ViewData["Category"] = category.Name;

            return View(post);
        }

        // GET: Post/Create
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]

        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "Id", "Name");
            return View();
        }

        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Title,ShortDescription,PostContent,UrlSlug,Published,PostedOn,Modified,CategoryId,ViewCount,RateCount,TotalRate")] Post post)
        {
            post.Published = true;
            post.PostedOn = DateTime.Now;
            post.Modified = false;
            post.ViewCount = 0;
            post.TotalRate = 0;
            if (ModelState.IsValid)
            {
                _postRespository.AddPost(post);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Post/Edit/5
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE},  {SD.BLOGOWNER_ROLE}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _postRespository.FindPost((int)id);
            if (post == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        // POST: Post/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Title,ShortDescription,PostContent,UrlSlug,Published,PostedOn,Modified,CategoryId,ViewCount,RateCount,TotalRate")] Post post)
        {
            if (id != post.Id)
            {
                return NotFound();
            }
            post.Modified = true;
            if (ModelState.IsValid)
            {
                try
                {
                    _postRespository.UpdatePost(post);
                }
                catch (Exception)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_categoryRepository.GetAllCategories(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Post/Delete/5
        [Authorize(Roles = $"{SD.BLOGOWNER_ROLE}")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            _postRespository.DeletePost((int)id);

            return RedirectToAction(nameof(Index));
        }




        #region API call

        public IActionResult GetAllPostJson()
        {
            var lstPost = _postRespository.GetAllPosts();
            return Json(new { data = lstPost });
        }

        public IActionResult MostViewedPostsJson()
        {
            var lstPost = _postRespository.GetMostViewedPost(5);
            return Json(new { data = lstPost });
        }

        public IActionResult MostInterestedPostsJson()
        {
            var lstPost = _postRespository.GetAllPosts().OrderByDescending(post => post.TotalRate).Take(5);
            return Json(new { data = lstPost });
        }

        public IActionResult GetPublishedPostJson()
        {
            var lstPost = _postRespository.GetAllPosts().Where(x => x.Published);
            return Json(new { data = lstPost });
        }

        public IActionResult GetUnpublishedPostJson()
        {
            var lstPost = _postRespository.GetAllPosts().Where(x => !x.Published);
            return Json(new { data = lstPost });
        }
        #endregion
    }
}
