﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.View.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.USER_ROLE}, {SD.BLOGOWNER_ROLE}")]
    public class CommentController : Controller
    {
        private readonly CommentRespository _commentRespository;
        private readonly PostRepository _postRespository;

        public CommentController()
        {
            _commentRespository = new CommentRespository();
            _postRespository = new PostRepository();
        }

        // GET: Comment
        public IActionResult Index()
        {
            var lstComment = _commentRespository.GetAllComments();
            return View(lstComment);
        }

        // GET: Comment/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRespository.FindComment((int)id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // GET: Comment/Create
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        public IActionResult Create()
        {
            ViewData["PostId"] = new SelectList(_postRespository.GetAllPosts(), "Id", "Title");
            return View();
        }

        // POST: Comment/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,Email,CommentHeader,CommentText,CommentTime,PostId")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                _commentRespository.AddComment(comment);
                return RedirectToAction(nameof(Index));
            }
            ViewData["PostId"] = new SelectList(_postRespository.GetAllPosts(), "Id", "Title", comment.PostId);
            return View(comment);
        }

        // GET: Comment/Edit/5
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE},  {SD.BLOGOWNER_ROLE}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRespository.FindComment((int)id);
            if (comment == null)
            {
                return NotFound();
            }
            ViewData["PostId"] = new SelectList(_postRespository.GetAllPosts(), "Id", "Title", comment.PostId);
            return View(comment);
        }

        // POST: Comment/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE},{SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,Email,CommentHeader,CommentText,CommentTime,PostId")] Comment comment)
        {
            if (id != comment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _commentRespository.UpdateComment(comment);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PostId"] = new SelectList(_postRespository.GetAllPosts(), "Id", "Title", comment.PostId);
            return View(comment);
        }

        // GET: Comment/Delete/5
        [Authorize(Roles = $"{SD.BLOGOWNER_ROLE}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRespository.FindComment((int)id);
            if (comment == null)
            {
                return NotFound();
            }
            _commentRespository.DeleteComment(comment);

            return RedirectToAction(nameof(View));
        }
        #region API call
        // GET: Comment/View

        public IActionResult GetAllCommentsJson()
        {
            var lstComments = _commentRespository.GetAllComments();
            return Json(new { data = lstComments });
        }
        #endregion
    }
}
