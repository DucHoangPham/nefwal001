﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.View.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.USER_ROLE}, {SD.BLOGOWNER_ROLE}")]
    public class TagController : Controller
    {
        private readonly TagRepository _tagRepository;

        public TagController()
        {
            _tagRepository = new TagRepository();
        }

        // GET: Tag
        public IActionResult Index()
        {
            var lstTag = _tagRepository.GetAllTags();
            return View(lstTag);
        }

        // GET: Tag/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tag = _tagRepository.Find((int)id);

            return View(tag);
        }

        //Get tag by name
        public IActionResult GetTagByName(string name)
        {
            var tag = _tagRepository.GetTagByUrlSlug(name);
            return View("Details", tag);
        }

        // GET: Tag/Create
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        public IActionResult Create()
        {
            return View();
        }



        // POST: Tag/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,UrlSlug,Description,Count")] Tag tag)
        {
            tag.Count = 0;
            if (ModelState.IsValid)
            {
                _tagRepository.AddTag(tag);
                return RedirectToAction(nameof(Index));
            }
            return View(tag);
        }

        // GET: Tag/Edit/5
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tag = _tagRepository.Find((int)id);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        // POST: Tag/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = $"{SD.CONTRIBUTER_ROLE}, {SD.BLOGOWNER_ROLE}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,UrlSlug,Description,Count")] Tag tag)
        {
            if (id != tag.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _tagRepository.UpdateTag(tag);
                }
                catch
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tag);
        }

        // GET: Tag/Delete/5
        [Authorize(Roles = $"{SD.BLOGOWNER_ROLE}")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tag = _tagRepository.Find((int)id);
            if (tag == null)
            {
                return NotFound();
            }
            _tagRepository.DeleteTag(tag);

            return RedirectToAction(nameof(Index));
        }
        #region Get API


        public IActionResult GetAllTagsJson()
        {
            var lstTag = _tagRepository.GetAllTags();
            return Json(new { data = lstTag });
        }
        #endregion

    }
}
