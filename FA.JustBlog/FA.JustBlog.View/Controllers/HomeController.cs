using FA.JustBlog.Core.Repositories;
using FA.JustBlog.View.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace FA.JustBlog.View.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PostRepository _postRespository;
        private readonly CategoryRepository _categoryRepository;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _categoryRepository = new CategoryRepository();
            _postRespository = new PostRepository();
        }

        public IActionResult Index()
        {
            var lstPost = _postRespository.GetAllPosts();
            return View(lstPost);
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }

        //Get post order by PostOn
        public IActionResult PostOrderByPostOn()
        {
            var lstPost = _postRespository.GetAllPosts().OrderBy(x => x.PostedOn);
            return View("Index", lstPost);
        }

        //Get MostViewedPosts
        public IActionResult MostViewedPosts()
        {
            var lstPost = _postRespository.GetMostViewedPost(5);
            return View("Index", lstPost);
        }

        //Get most 5 interested post
        public IActionResult MostInterestedPosts()
        {
            var lstPost = _postRespository.GetAllPosts().OrderByDescending(post => post.TotalRate).Take(5);
            return View("Index", lstPost);
        }

        //Get published post
        public IActionResult GetPublishedPost()
        {
            var lstPost = _postRespository.GetAllPosts().Where(x => x.Published);
            return View("Index", lstPost);
        }

        //Get Unpublished post
        public IActionResult GetUnpublishedPost()
        {
            var lstPost = _postRespository.GetAllPosts().Where(x => !x.Published);
            return View("Index", lstPost);
        }

        //Get post by category
        public IActionResult PostByCategory(string categoryName)
        {
            var lstPost = _postRespository.GetPostsByCategory(categoryName);
            return View("Index", lstPost);
        }

        //Get post by year month
        [HttpGet("post/{year:int}/{month:int}/{title}")]
        public IActionResult PostByMonthYear(int year, int month, string title)
        {
            var post = _postRespository.FindPost(year, month, title);
            if (post == null)
            {
                return NotFound();
            }
            var category = _categoryRepository.Find(post.CategoryId);
            ViewData["Category"] = category.Name;
            return View("Details", post);
        }



        //Get post by tag
        public IActionResult GetPostByTag(string tagName)
        {
            var lstPost = _postRespository.GetPostsByTag(tagName);
            return View("Index", lstPost);
        }



        // GET: Post/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = _postRespository.FindPost((int)id);
            var category = _categoryRepository.Find(post.CategoryId);
            ViewData["Category"] = category.Name;

            return View(post);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public ActionResult AboutCard()
        {
            return PartialView("_PartialAboutCard");
        }
        public ActionResult ListPostMostView()
        {
            var lstPost = _postRespository.GetMostViewedPost(5);
            return PartialView("_ListPosts", lstPost);
        }
    }
}
