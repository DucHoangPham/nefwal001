﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.View.ViewComponents
{
    public class ListPostViewComponent : ViewComponent
    {
        private readonly PostRepository _postRespository;
        public ListPostViewComponent()
        {
            _postRespository = new PostRepository();
        }
        public IViewComponentResult Invoke()
        {
            var lstPost = _postRespository.GetHighestPosts(5);
            return View(lstPost);
        }
    }
}
