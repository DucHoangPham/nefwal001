﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.View.ViewComponents
{
    public class ListTagTopTenViewComponent : ViewComponent
    {
        private readonly TagRepository _tagRepository;
        public ListTagTopTenViewComponent()
        {
            _tagRepository = new TagRepository();
        }
        public IViewComponentResult Invoke()
        {
            var tags = _tagRepository.GetTopTag(10);
            return View(tags);
        }
    }
}
