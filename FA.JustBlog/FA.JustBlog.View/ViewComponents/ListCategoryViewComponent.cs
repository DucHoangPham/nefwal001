﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.View.ViewComponents
{
    public class ListCategoryViewComponent : ViewComponent
    {
        private readonly CategoryRepository _categoryRepository;
        public ListCategoryViewComponent()
        {
            _categoryRepository = new CategoryRepository();
        }
        public IViewComponentResult Invoke()
        {
            var categories = _categoryRepository.GetAllCategories();
            return View(categories);
        }
    }
}
