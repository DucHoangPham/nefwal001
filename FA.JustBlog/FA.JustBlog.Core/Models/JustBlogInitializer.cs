﻿using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Models
{
    public static class JustBlogInitializer
    {
        public static void Seed(this ModelBuilder modelbuilder)
        {
            int firstCategoryId = 1;
            int secondCategoryId = 2;
            int thirdCategoryId = 3;
            int firstTagId = 1;
            int secondTagId = 2;
            int thirdTagId = 3;
            int firstPostId = 1;
            int secondPostId = 2;
            int thirdPostId = 3;
            modelbuilder.Entity<Category>().HasData(
                new Category
                {
                    Id = firstCategoryId,
                    Name = "C",
                    UrlSlug = "csharp",
                    Description = "C programming language",
                },
                new Category
                {
                    Id = secondCategoryId,
                    Name = "ASP NET",
                    UrlSlug = "asp net",
                    Description = "ASP NET web development",
                },
                new Category
                {
                    Id = thirdCategoryId,
                    Name = "SQL Server",
                    UrlSlug = "sql server",
                    Description = "SQL Server database",
                });
            modelbuilder.Entity<Tag>().HasData(
                new Tag
                {
                    Id = firstTagId,
                    Name = "C ",
                    UrlSlug = "c sharp",
                    Description = "C programming language",
                    Count = 1,
                },
                new Tag
                {
                    Id = secondTagId,
                    Name = "ASP NET",
                    UrlSlug = "asp net",
                    Description = "ASP NET web development",
                    Count = 1,
                },
                new Tag
                {
                    Id = thirdTagId,
                    Name = "SQL Server",
                    UrlSlug = "sql server",
                    Description = "SQL Server database",
                    Count = 1,
                });
            modelbuilder.Entity<Post>().HasData(
                new Post
                {
                    Id = firstPostId,
                    Title = "C  Programming",
                    ShortDescription = "C  programming language",
                    PostContent = "C  programming language",
                    UrlSlug = "c sharp programming",
                    Published = true,
                    PostedOn = DateTime.Now,
                    CategoryId = firstCategoryId,
                },
                new Post
                {
                    Id = secondPostId,
                    Title = "ASPNET Web Development",
                    ShortDescription = "ASPNET web development",
                    PostContent = "ASPNET web development",
                    UrlSlug = "asp net web development",
                    Published = true,
                    PostedOn = DateTime.Now,
                    CategoryId = secondCategoryId,
                },
                new Post
                {
                    Id = thirdPostId,
                    Title = "SQL Server Database",
                    ShortDescription = "SQL Server database",
                    PostContent = "SQL Server database",
                    UrlSlug = "sql server database",
                    Published = true,
                    PostedOn = DateTime.Now,
                    CategoryId = thirdCategoryId,
                });
            modelbuilder.Entity<PostTagMap>().HasData(
                new PostTagMap
                {
                    PostId = firstPostId,
                    TagId = firstTagId,
                },
                new PostTagMap
                {
                    PostId = secondPostId,
                    TagId = secondTagId,
                },
                new PostTagMap
                {
                    PostId = thirdPostId,
                    TagId = thirdTagId,
                },
                new PostTagMap
                {
                    PostId = firstPostId,
                    TagId = secondTagId,
                });
        }
    }
}
