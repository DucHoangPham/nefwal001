﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Core.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Category name is required.")]
        [StringLength(255)]
        [RegularExpression(@"^[\d\w\s]+$", ErrorMessage = "The field must contain only letters, numbers, and spaces.")]
        public string Name { get; set; }
        [StringLength(255)]
        [RegularExpression(@"^[\d\w\s]+$", ErrorMessage = "The field must contain only letters, numbers, and spaces.")]
        public string UrlSlug { get; set; }
        [StringLength(1024)]
        [RegularExpression(@"^[\d\w\s]+$", ErrorMessage = "The field must contain only letters, numbers, and spaces.")]
        public string Description { get; set; }
        [ValidateNever]
        public virtual IList<Post> Posts { get; set; }
    }
}
