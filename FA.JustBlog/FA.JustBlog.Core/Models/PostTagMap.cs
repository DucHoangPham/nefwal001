﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace FA.JustBlog.Core.Models
{
    public class PostTagMap
    {
        public int PostId { get; set; }
        public int TagId { get; set; }
        [ValidateNever]
        public Post Post { get; set; }
        [ValidateNever]
        public Tag Tag { get; set; }
    }
}
