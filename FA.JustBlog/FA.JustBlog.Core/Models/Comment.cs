﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.JustBlog.Core.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(255)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        [StringLength(255)]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Comment header is required.")]
        [StringLength(255)]
        public string CommentHeader { get; set; }
        [Required(ErrorMessage = "Comment text is required.")]
        [StringLength(4000)]
        public string CommentText { get; set; }
        public DateTime CommentTime { get; set; } = DateTime.Now;
        [ForeignKey("Post")]
        public int PostId { get; set; }
        [ValidateNever]
        public virtual Post Post { get; set; }
    }
}
