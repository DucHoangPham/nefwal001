﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FA.JustBlog.Core.Models
{
    public class JustBlogContext : IdentityDbContext<IdentityUser>
    {
        public JustBlogContext()
        {

        }
        public JustBlogContext(DbContextOptions<JustBlogContext> options) : base(options)
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTagMap> PostTagsMap { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-H640EI8\\SQLEXPRESS;Initial Catalog=JustBlog;Integrated Security=True;TrustServerCertificate=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().ToTable("Posts");
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Tag>().ToTable("Tags");
            modelBuilder.Entity<PostTagMap>().ToTable("PostTagMap").HasKey(x => new
            {
                x.TagId,
                x.PostId
            });
            modelBuilder.Entity<PostTagMap>().HasOne(x => x.Post).WithMany(x => x.PostTagMaps).HasForeignKey(x => x.PostId);
            modelBuilder.Entity<PostTagMap>().HasOne(x => x.Tag).WithMany(x => x.PostTagMaps).HasForeignKey(x => x.TagId);
            modelBuilder.Entity<Comment>().ToTable("Comments");
            modelBuilder.Seed();
        }
    }

}
