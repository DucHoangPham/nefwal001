﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA.JustBlog.Core.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Post title is required.")]
        [StringLength(255)]
        public string Title { get; set; }
        [StringLength(1024)]
        [Column("Short Description")]
        public string ShortDescription { get; set; }
        [StringLength(4000)]
        [Column("Post Content")]
        public string PostContent { get; set; }
        [StringLength(255)]
        public string UrlSlug { get; set; }
        public bool Published { get; set; } = false;
        [Column("Posted On")]
        public DateTime PostedOn { get; set; }
        public bool Modified { get; set; } = false;
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public int ViewCount { get; set; }
        public int RateCount { get; set; } = 1;
        public int TotalRate { get; set; }
        [ValidateNever]
        public virtual Category Category { get; set; }
        [ValidateNever]
        public virtual IList<PostTagMap> PostTagMaps { get; set; }
        [ValidateNever]
        public virtual IList<Comment> Comments { get; set; }
    }
}
