﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public interface ICommentRespository
    {
        Comment FindComment(int commentId);
        void AddComment(Comment comment);
        void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody);
        void UpdateComment(Comment comment);
        void DeleteComment(int commentId);
        void DeleteComment(Comment comment);
        IList<Comment> GetAllComments();
        IList<Comment> GetCommentsForPost(int postId);
        IList<Comment> GetCommentsForPost(Post post);
    }
}
