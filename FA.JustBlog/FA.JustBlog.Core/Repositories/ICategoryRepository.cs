﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public interface ICategoryRepository
    {
        Category Find(int categoryId);
        Category Find(string name);
        void AddCategory(Category category);
        void UpdateCategory(Category category);
        void DeleteCategory(int categoryId);
        void DeleteCategory(Category category);
        IList<Category> GetAllCategories();
    }
}
