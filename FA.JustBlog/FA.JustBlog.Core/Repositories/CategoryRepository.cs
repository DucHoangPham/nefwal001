﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly JustBlogContext _context;
        public CategoryRepository()
        {
            _context = new JustBlogContext();
        }
        public void AddCategory(Category category)
        {
            try
            {
                string regex = @"^[\d\w\s]+$";
                if (!Utility.CheckRegex(category.Name, regex) || !Utility.CheckRegex(category.UrlSlug, regex) || !Utility.CheckRegex(category.Description, regex))
                {
                    throw new Exception();
                }
                _context.Categories.Add(category);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public void DeleteCategory(int categoryId)
        {
            try
            {
                var categoryDelete = _context.Categories.Find(categoryId);
                IQueryable<Post> postDelete = _context.Posts.Where(x => x.CategoryId == categoryDelete!.Id);
                _context.Posts.RemoveRange(postDelete);
                _context.Categories.Remove(categoryDelete!);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeleteCategory(Category category)
        {
            try
            {
                IQueryable<Post> postDelete = _context.Posts.Where(x => x.CategoryId == category.Id);
                _context.Posts.RemoveRange(postDelete);
                _context.Categories.Remove(category);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Category Find(int categoryId)
        {
            try
            {
                var category = _context.Categories.FirstOrDefault(x => x.Id == categoryId);
                if (category == null)
                {
                    throw new ArgumentNullException();
                }
                return category!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Category Find(string name)
        {
            try
            {
                return _context.Categories.FirstOrDefault(x => x.Name.ToLower().Equals(name.ToLower()));
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Category> GetAllCategories()
        {
            var categories = _context.Categories.ToList();
            return categories;
        }

        public void UpdateCategory(Category category)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                var categoryUpdate = _context.Categories.FirstOrDefault(x => x.Id == category.Id);
                if (!Utility.CheckRegex(category.Name, regex) || !Utility.CheckRegex(category.UrlSlug, regex) || !Utility.CheckRegex(category.Description, regex))
                {
                    throw new Exception();
                }
                categoryUpdate!.UrlSlug = category.UrlSlug;
                categoryUpdate.Name = category.Name;
                categoryUpdate.Description = category.Description;
                _context.Categories.Update(categoryUpdate);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

    }
}
