﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public interface ITagRepository
    {
        Tag Find(int tagId);
        void AddTag(Tag tag);
        void UpdateTag(Tag tag);
        void DeleteTag(int tagId);
        void DeleteTag(Tag tag);
        IList<Tag> GetAllTags();
        Tag GetTagByUrlSlug(string urlSlug);
        IList<Tag> GetTopTag(int size);
    }
}
