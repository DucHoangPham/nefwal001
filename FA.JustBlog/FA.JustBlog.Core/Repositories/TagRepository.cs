﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly JustBlogContext _context;
        public TagRepository()
        {
            _context = new JustBlogContext();
        }
        public void AddTag(Tag tag)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                if (!Utility.CheckRegex(tag.Name, regex) || !Utility.CheckRegex(tag.UrlSlug, regex) || !Utility.CheckRegex(tag.Description, regex))
                {
                    throw new Exception();
                }
                _context.Tags.Add(tag);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeleteTag(int tagId)
        {
            try
            {
                var tagDelete = _context.Tags.FirstOrDefault(x => x.Id == tagId);
                var lstPostTag = _context.PostTagsMap.Where(x => x.TagId == tagDelete!.Id);
                _context.PostTagsMap.RemoveRange(lstPostTag);
                _context.Tags.Remove(tagDelete!);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeleteTag(Tag tag)
        {
            try
            {
                var lstPostTag = _context.PostTagsMap.Where(x => x.TagId == tag.Id);
                _context.PostTagsMap.RemoveRange(lstPostTag);
                _context.Tags.Remove(tag);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Tag Find(int tagId)
        {
            try
            {
                var result = _context.Tags.FirstOrDefault(x => x.Id == tagId);
                if (result == null)
                {
                    throw new Exception();
                }
                return result!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Tag> GetAllTags()
        {
            return _context.Tags.ToList();
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            try
            {
                var tag = _context.Tags.FirstOrDefault(x => x.UrlSlug.ToLower() == urlSlug.ToLower());
                return tag!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Tag> GetTopTag(int size)
        {
            try
            {
                var lstTag = _context.Tags.Join(_context.PostTagsMap, tag => tag.Id, postTag => postTag.TagId, (tag, postTag) => new { tag, postTag }).GroupBy(x => x.tag.Id).Select(x => new { x.Key, Count = x.Count() }).OrderByDescending(x => x.Count).Take(size).Select(x => x.Key).ToList();
                var result = new List<Tag>();
                foreach (var item in lstTag)
                {
                    result.Add(_context.Tags.FirstOrDefault(x => x.Id == item)!);
                }
                return result;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void UpdateTag(Tag tag)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                var tagUpdate = _context.Tags.FirstOrDefault(x => x.Id == tag.Id);
                if (!Utility.CheckRegex(tag.Name, regex) || !Utility.CheckRegex(tag.UrlSlug, regex) || !Utility.CheckRegex(tag.Description, regex))
                {
                    throw new Exception();
                }
                tagUpdate!.Name = tag.Name;
                tagUpdate.Description = tag.Description;
                tagUpdate.UrlSlug = tag.UrlSlug;
                tagUpdate.Description = tag.Description;
                tagUpdate.Count = tag.Count;
                _context.Tags.Update(tagUpdate);
                _context.SaveChanges();

            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

    }
}
