﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public class CommentRespository : ICommentRespository
    {
        private readonly JustBlogContext _context;
        public CommentRespository()
        {
            _context = new JustBlogContext();
        }
        public void AddComment(Comment comment)
        {
            string regex = @"^[\d\w\s]+$";
            string emailRegex = @"^[\w\d]+@[\w\d]+\.[\w\d]+$";

            try
            {
                if (!Utility.CheckRegex(comment.Name, regex) || !Utility.CheckRegex(comment.Email, emailRegex) || !Utility.CheckRegex(comment.CommentHeader, regex) || !Utility.CheckRegex(comment.CommentText, regex))
                {
                    throw new Exception();
                }
                _context.Comments.Add(comment);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            string regex = @"^[\d\w\s]+$";
            string emailRegex = @"^[\w\d]+@[\w\d]+\.[\w\d]+$";
            try
            {
                if (!Utility.CheckRegex(commentName, regex) || !Utility.CheckRegex(commentEmail, emailRegex) || !Utility.CheckRegex(commentTitle, regex) || !Utility.CheckRegex(commentBody, regex))
                {
                    throw new Exception();
                }
                Comment comment = new Comment()
                {
                    PostId = postId,
                    Name = commentName,
                    Email = commentEmail,
                    CommentHeader = commentTitle,
                    CommentText = commentBody,
                    CommentTime = DateTime.Now
                };
                _context.Comments.Add(comment);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeleteComment(int commentId)
        {
            try
            {
                var commentDelete = _context.Comments.FirstOrDefault(x => x.Id == commentId);
                if (commentDelete == null)
                {
                    throw new Exception();
                }
                _context.Comments.Remove(commentDelete!);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeleteComment(Comment comment)
        {
            try
            {
                _context.Comments.Remove(comment);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Comment FindComment(int commentId)
        {
            try
            {
                var commentFind = _context.Comments.FirstOrDefault(x => x.Id == commentId);
                if (commentFind == null)
                {
                    throw new Exception();
                }
                return commentFind!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Comment> GetAllComments()
        {
            return _context.Comments.ToList();
        }

        public IList<Comment> GetCommentsForPost(int postId)
        {
            try
            {
                return _context.Comments.Where(x => x.PostId == postId).ToList();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Comment> GetCommentsForPost(Post post)
        {
            try
            {
                var postFind = _context.Posts.FirstOrDefault(x => x == post);
                if (postFind == null)
                {
                    throw new Exception();
                }
                return _context.Comments.Where(x => x.PostId == postFind.Id).ToList();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void UpdateComment(Comment comment)
        {
            string regex = @"^[\d\w\s]+$";
            string emailRegex = @"^[\w\d]+@[\w\d]+\.[\w\d]+$";
            try
            {
                if (!Utility.CheckRegex(comment.Name, regex) || !Utility.CheckRegex(comment.Email, emailRegex) || !Utility.CheckRegex(comment.CommentHeader, regex) || !Utility.CheckRegex(comment.CommentText, regex))
                {
                    throw new Exception();
                }
                var commentUpdate = _context.Comments.FirstOrDefault(x => x.Id == comment.Id);
                if (commentUpdate == null)
                {
                    throw new Exception();
                }
                commentUpdate.Name = comment.Name;
                commentUpdate.Email = comment.Email;
                commentUpdate.CommentHeader = comment.CommentHeader;
                commentUpdate.CommentText = comment.CommentText;
                commentUpdate.CommentTime = comment.CommentTime;
                commentUpdate.PostId = comment.PostId;
                _context.Comments.Update(commentUpdate);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }
    }
}
