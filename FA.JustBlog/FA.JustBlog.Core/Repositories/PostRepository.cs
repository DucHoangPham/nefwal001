﻿using FA.JustBlog.Core.Models;

namespace FA.JustBlog.Core.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly JustBlogContext _context;
        public PostRepository()
        {
            _context = new JustBlogContext();
        }
        public void AddPost(Post post)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                if (!Utility.CheckRegex(post.Title, regex) || !Utility.CheckRegex(post.UrlSlug, regex) || !Utility.CheckRegex(post.ShortDescription, regex) || !Utility.CheckRegex(post.PostContent, regex))
                {
                    throw new Exception();
                }
                _context.Posts.Add(post);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public int CountPostsForCategory(string category)
        {
            try
            {
                Category categoryFind = _context.Categories.FirstOrDefault(x => x.Name.Trim().ToLower() == category.Trim().ToLower())!;
                var count = _context.Posts.Where(x => x.CategoryId == categoryFind.Id).Count();
                return count;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public int CountPostsForTag(string tag)
        {
            try
            {
                Tag tagFind = _context.Tags.FirstOrDefault(x => x.Name.Trim().ToLower() == tag.Trim().ToLower())!;
                var count = _context.PostTagsMap.Where(x => x.TagId == tagFind.Id).Count();
                return count;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void DeletePost(int postId)
        {
            try
            {
                var postDelete = _context.Posts.FirstOrDefault(x => x.Id == postId)!;
                List<PostTagMap> lstPostTagDelete = _context.PostTagsMap.Where(x => x.PostId == postDelete.Id).ToList();
                List<Comment> lstComments = _context.Comments.Where(x => x.PostId == postDelete.Id).ToList();
                _context.Comments.RemoveRange(lstComments);
                _context.PostTagsMap.RemoveRange(lstPostTagDelete);
                _context.Posts.Remove(postDelete);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public void DeletePost(Post post)
        {
            try
            {
                List<PostTagMap> lstPostTagDelete = _context.PostTagsMap.Where(x => x.PostId == post.Id).ToList();
                List<Comment> lstComments = _context.Comments.Where(x => x.PostId == post.Id).ToList();
                _context.Comments.RemoveRange(lstComments);
                _context.PostTagsMap.RemoveRange(lstPostTagDelete);
                _context.Posts.Remove(post);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            try
            {
                var postFind = _context.Posts.FirstOrDefault(x => x.PostedOn.Year == year && x.PostedOn.Month == month && x.UrlSlug.ToLower() == urlSlug.ToLower());
                if (postFind == null)
                {
                    throw new Exception();
                }
                return postFind!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public Post FindPost(int postId)
        {
            try
            {
                var postFind = _context.Posts.FirstOrDefault(x => x.Id == postId);
                if (postFind == null)
                {
                    throw new Exception();
                }
                return postFind!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetAllPosts()
        {
            return _context.Posts.ToList();
        }

        public IList<Post> GetHighestPosts(int size)
        {
            try
            {
                var lstPostResult = _context.Posts.Select(x => new
                {
                    Post = x,
                    TotalRate = (decimal)(x.TotalRate * 1.0 / x.RateCount)
                }).OrderByDescending(x => x.TotalRate).Take(size).Select(x => x.Post).ToList();
                return lstPostResult;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetLatestPost(int size)
        {
            try
            {
                var result = _context.Posts.OrderByDescending(x => x.PostedOn).Take(size).ToList();
                return result;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetMostViewedPost(int size)
        {
            try
            {
                var lstPostResult = _context.Posts.OrderByDescending(x => x.ViewCount).Take(size).ToList();
                return lstPostResult;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetPostsByCategory(string category)
        {
            try
            {
                Category categoryFind = _context.Categories.FirstOrDefault(x => x.Name.Trim().ToLower() == category.Trim().ToLower())!;
                if (categoryFind == null)
                {
                    throw new Exception();
                }
                var result = _context.Posts.Where(x => x.CategoryId == categoryFind.Id).ToList();
                return result;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetPostsByMonth(DateTime monthYear)
        {
            try
            {
                var lstPost = _context.Posts.Where(x => x.PostedOn.Month == monthYear.Month).ToList();
                return lstPost;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetPostsByTag(string tag)
        {
            try
            {
                var normalizedTagName = tag.Trim().ToLower();

                var posts = _context.Posts
                    .Where(p => p.PostTagMaps.Any(pt => pt.Tag.Name.Trim().ToLower() == normalizedTagName))
                    .ToList();

                return posts;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetPublishedPosts()
        {
            try
            {
                return _context.Posts.Where(x => x.Published).ToList();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public IList<Post> GetUnpublisedPosts()
        {
            try
            {
                return _context.Posts.Where(x => !x.Published).ToList();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        public void UpdatePost(Post post)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                if (!Utility.CheckRegex(post.Title, regex) || !Utility.CheckRegex(post.UrlSlug, regex) || !Utility.CheckRegex(post.ShortDescription, regex) || !Utility.CheckRegex(post.PostContent, regex))
                {
                    throw new Exception();
                }
                var postUpdate = _context.Posts.FirstOrDefault(x => x.Id == post.Id)!;
                postUpdate.Title = post.Title;
                postUpdate.ShortDescription = post.ShortDescription;
                postUpdate.PostContent = post.PostContent;
                postUpdate.UrlSlug = post.UrlSlug;
                postUpdate.Published = post.Published;
                postUpdate.PostedOn = post.PostedOn;
                postUpdate.Modified = true;
                postUpdate.CategoryId = post.CategoryId;
                _context.Posts.Update(postUpdate);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }
        public Category GetCategoryByPostId(int id)
        {
            try
            {
                var categoryId = _context.Posts.FirstOrDefault(x => x.Id == id).CategoryId!;
                var category = _context.Categories.FirstOrDefault(x => x.Id == categoryId);
                return category!;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }
        public IList<Tag> GetTagsForPost(int id)
        {
            try
            {
                var lstTagId = _context.PostTagsMap.Where(x => x.PostId == id).Select(x => x.TagId).ToList();
                var lstTag = _context.Tags.Where(x => lstTagId.Contains(x.Id)).ToList();
                return lstTag;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }
    }
}
