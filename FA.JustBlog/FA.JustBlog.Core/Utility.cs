﻿namespace FA.JustBlog.Core
{
    public static class Utility
    {
        public static bool CheckRegex(string input, string regex)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(input, regex);
        }
    }
}
